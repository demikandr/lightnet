#!/usr/bin/env python
#
#   Copyright EAVISE
#   Example: Perform a single image detection with the Lightnet yolo network
#

import os
import argparse
import logging
import cv2
import torch
from torchvision import transforms as tf
import brambox.boxes as bbb
import lightnet as ln

log = logging.getLogger('lightnet.detect')

# Parameters
LABELS = ['speed limit 20 (prohibitory)', 'speed limit 30 (prohibitory)', 'speed limit 50 (prohibitory)',
          'speed limit 60 (prohibitory)', 'speed limit 70 (prohibitory)', 'speed limit 80 (prohibitory)',
          'restriction ends 80 (other)', 'speed limit 100 (prohibitory)', 'speed limit 120 (prohibitory)',
          'no overtaking (prohibitory)', 'no overtaking (trucks) (prohibitory)', 'priority at next intersection (danger)',
          'priority road (other)', 'give way (other)', 'stop (other)', 'no traffic both ways (prohibitory)',
          'no trucks (prohibitory)', 'no entry (other)', 'danger (danger)', 'bend left (danger)', 'bend right (danger)',
          'bend (danger)', 'uneven road (danger)', 'slippery road (danger)', 'road narrows (danger)', 'construction (danger)',
          'traffic signal (danger)', 'pedestrian crossing (danger)', 'school crossing (danger)', 'cycles crossing (danger)',
          'snow (danger)', 'animals (danger)', 'restriction ends (other)', 'go right (mandatory)', 'go left (mandatory)',
          'go straight (mandatory)', 'go right or straight (mandatory)', 'go left or straight (mandatory)',
          'keep right (mandatory)', 'keep left (mandatory)', 'roundabout (mandatory)', 'restriction ends (overtaking) (other)',
          'restriction ends (overtaking (trucks)) (other)']
CLASSES=43
ANCHORS = [(1.3221, 1.3221), (3.19275, 3.19275), (5.05587, 5.05587), (9.47112, 9.47112), (10.0071, 10.0071)]

NETWORK_SIZE = [1344, 800] # [416, 416]
CONF_THRESH = .25
NMS_THRESH = .4


# Functions
def create_network():
    """ Create the lightnet network """
    net = ln.models.Yolo(CLASSES, args.weight, CONF_THRESH, NMS_THRESH, anchors=ANCHORS)
    net.postprocess.append(ln.data.transform.TensorToBrambox(NETWORK_SIZE, LABELS))

    net.eval()
    if args.cuda:
        net.cuda()

    return net


def 3detect(net, img_path):
    """ Perform a detection """
    # Load image
    img = cv2.imread(img_path)
    im_h, im_w = img.shape[:2]

    img_tf = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img_tf = ln.data.transform.Letterbox.apply(img_tf, dimension=NETWORK_SIZE)
    img_tf = tf.ToTensor()(img_tf)
    img_tf.unsqueeze_(0)
    if args.cuda:
        img_tf = img_tf.cuda()
    
    # Run detector
    if torch.__version__.startswith('0.3'):
        img_tf = torch.autograd.Variable(img_tf, volatile=True)
        out = net(img_tf)
    else:
        with torch.no_grad():
            out = net(img_tf)
    out = ln.data.transform.ReverseLetterbox.apply(out, NETWORK_SIZE, (im_w, im_h))

    return img, out


# Main
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run an image through the lightnet yolo network')
    parser.add_argument('weight', help='Path to weight file')
    parser.add_argument('image', help='Path to image file(s)', nargs='*')
    parser.add_argument('-c', '--cuda', action='store_true', help='Use cuda')
    parser.add_argument('-s', '--save', action='store_true', help='Save image in stead of displaying it')
    parser.add_argument('-l', '--label', action='store_true', help='Print labels and scores on the image')
    args = parser.parse_args()

    # Parse Arguments
    if args.cuda and not torch.cuda.is_available():
        log.error('CUDA not available')
        args.cuda = False

    # Network
    network = create_network()
    print()

    # Detection
    if len(args.image) > 0:
        for img_name in args.image:
            log.info(img_name)
            image, output = detect(network, img_name)

            bbb.draw_boxes(image, output[0], show_labels=args.label)
            if args.save:
                cv2.imwrite('detections.png', image)
            else:
                cv2.imshow('image', image)
                cv2.waitKey(0)
                cv2.destroyAllWindows()
    else:
        while True:
            try:
                img_path = input('Enter image path: ')    
            except (KeyboardInterrupt, EOFError):
                print('')
                break
        
            if not os.path.isfile(img_path):
                log.error(f'\'{img_path}\' is not a valid path')
                break

            image, output = detect(network, img_path)
            bbb.draw_boxes(image, output[0], show_labels=args.label)
            if args.save:
                cv2.imwrite('detections.png', image)
            else:
                cv2.imshow('image', image)
                cv2.waitKey(0)
                cv2.destroyAllWindows()
