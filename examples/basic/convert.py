from collections import defaultdict
import os

def convert(gt, outputPath, H, W):
    detections = defaultdict(list)
    with open(gt) as file:
        for line in file.readlines(): # use only images with traffic signs
            line = line.strip()
            row = line.split(';')
            img, label = (row[0], row[-1])
            x1, y1, x2, y2 = [int(cell) for cell in row[1:-1]]
            x, y = (x1 / H, y1 / W)
            height, width = (x2 - x1) / H, (y2 - y1) / W
            detections[img].append((label, x, y, height, width))

    for img in detections:
        with open(os.path.join(outputPath, "images", os.path.splitext(img)[0] + '.txt'), 'w') as annotationsFile:
            annotationsString = '\n'.join((' '.join(map(str, x)) for x in detections[img]))
            annotationsFile.write(annotationsString)

    with open(os.path.join(outputPath, "training_list.txt"), 'w') as imgsListFile:
        imgsListString = '\n'.join((os.path.join(outputPath, 'images', img) for img in detections))
        imgsListFile.write(imgsListString)

gt = '/home/demikandr/SideProjects/SignDetector/data/TrainIJCNN2013/gt.txt'
outputPath = '/home/demikandr/SideProjects/SignDetector/data/train'
H, W = [1360, 800]

if __name__ == '__main__':
    convert(gt, outputPath, H, W)

with open('interception.txt') as inp:
    T = int(inp.readline().strip())
    for testIdx in range(T):
        N = int(inp.readline().strip())
        zeroto = 1
        nonzeroto = 1
        b = int(int(inp.readline().strip()) != 0)
        for i in range(N):
            inp.readline()
        zeroroot = (1 + N + b) % 2
        print('Case #{}: {}'.format(testIdx + 1, zeroroot))
        if zeroroot == 1:
            print('0.0')