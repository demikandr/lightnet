import os
from flask import Flask, render_template, request
import cv2
from cv2 import resize
import matplotlib
from time import ctime
import pickle

matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

from matplotlib.patches import Rectangle

app = Flask(__name__)

UPLOAD_FOLDER = os.path.basename('uploads')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
LABELS = ['speed limit 20 (prohibitory)', 'speed limit 30 (prohibitory)', 'speed limit 50 (prohibitory)',
          'speed limit 60 (prohibitory)', 'speed limit 70 (prohibitory)', 'speed limit 80 (prohibitory)',
          'restriction ends 80 (other)', 'speed limit 100 (prohibitory)', 'speed limit 120 (prohibitory)',
          'no overtaking (prohibitory)', 'no overtaking (trucks) (prohibitory)', 'priority at next intersection (danger)',
          'priority road (other)', 'give way (other)', 'stop (other)', 'no traffic both ways (prohibitory)',
          'no trucks (prohibitory)', 'no entry (other)', 'danger (danger)', 'bend left (danger)', 'bend right (danger)',
          'bend (danger)', 'uneven road (danger)', 'slippery road (danger)', 'road narrows (danger)', 'construction (danger)',
          'traffic signal (danger)', 'pedestrian crossing (danger)', 'school crossing (danger)', 'cycles crossing (danger)',
          'snow (danger)', 'animals (danger)', 'restriction ends (other)', 'go right (mandatory)', 'go left (mandatory)',
          'go straight (mandatory)', 'go right or straight (mandatory)', 'go left or straight (mandatory)',
          'keep right (mandatory)', 'keep left (mandatory)', 'roundabout (mandatory)', 'restriction ends (overtaking) (other)',
          'restriction ends (overtaking (trucks)) (other)', 'None']
MODEL_COEF = np.load('../experiments/SVMdetector.npy')

def getHogDescriptor():
    hog = cv2.HOGDescriptor((32,32), (16,16), (8,8), (8,8), 9, 1)
    hog.setSVMDetector(MODEL_COEF)
    return hog

COLORS=['red', 'blue', 'orange', 'green', 'grey']

def drawROI(image, rois, filename, detectorOutput=False,):
    plt.imshow(image)
    currentAxis = plt.gca()
    for color, roi in zip(COLORS, rois):
        if detectorOutput:
            xy, w, h = (roi[0], roi[1]), roi[2], roi[3]
        else:
            xy, w, h = (roi[0], roi[1]), roi[2] - roi[0], roi[3] - roi[1]
        currentAxis.add_patch(Rectangle(xy, w, h, alpha=1, fill=None, color=color))
    plt.savefig(filename)
    plt.clf()


def detectorRoiToMyRoi(roi):
    return (roi[0], roi[1], roi[0] + roi[2], roi[1] + roi[3])

def getCrop(image, roi):
    return image[roi[1]:roi[3], roi[0]:roi[2]]

def toHOG(image, hog):
    image = resize(image, (32, 32))
    h = hog.compute(image.astype(np.uint8)).flatten()
    return h

def toHOGs(images, hog):
    hogs = list(map(lambda img: toHOG(img, hog), images))
    return hogs


def getClasses(image, rois, model):
    if len(rois) == 0:
        return []
    crops = []
    for roi in rois:
        roi = detectorRoiToMyRoi(roi)
        crops.append(getCrop(image, roi))
    hogs = toHOGs(crops, getHogDescriptor())
    return model.predict(hogs)

def loadModel(name):
    with open(name, 'rb') as f:
        model = pickle.load(f)
    return model



classificator = loadModel('../experiments/rfc.cpickle')

def filterNones(detections, scores, classes):
    detections = [detection for (detection, class_) in zip(detections, classes) if class_ != 43]
    scores = [score for (score, class_) in zip(scores, classes) if class_ != 43]
    classes = [class_ for class_ in classes if class_ != 43]
    return detections, scores, classes

@app.route('/')
def hello_world():
    return render_template('index.html')


@app.route('/upload', methods=['POST'])
def upload_file():
    file = request.files['image']
    f = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)

    # add your custom code to check that the uploaded file is a valid image and not a malicious file (out-of-scope for this post)
    file.save(f)
    img = cv2.imread(f)

    detections, scores = getHogDescriptor().detectMultiScale(img, 1)
    classes = getClasses(img, detections, classificator)
    detections, scores, classes = filterNones(detections, scores, classes)
    scores = [x[0] for x in scores]
    while len(scores) < 4:
        scores.append(0)
    while len(classes) < 4:
        classes.append(43)
    resultingImageFilename = os.path.join('static', 'tmpnochanceofcollision.png')
    drawROI(img, detections, resultingImageFilename, True)



    classes = [LABELS[class_] for class_ in classes]

    return render_template('index.html', detections=resultingImageFilename + '?{}'.format(ctime()),
                           detection0colour=COLORS[0], detection0score=str(scores[0]), detection0class=str(classes[0]),
                           detection1colour=COLORS[1], detection1score=str(scores[1]), detection1class=str(classes[1]),
                           detection2colour=COLORS[2], detection2score=str(scores[2]), detection2class=str(classes[2]),
                           detection3colour=COLORS[3], detection3score=str(scores[3]), detection3class=str(classes[3]))